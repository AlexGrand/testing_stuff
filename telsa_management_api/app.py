"""
This application creates Flask's instance 'app'
Registers url paths and run app.

Functions:
create_app
register_paths
app_run
"""

from flask import Flask


def create_app():
    """Create base Flask app, register paths

    :return: app - Flask instance"""
    app = Flask(__name__)
    register_paths(app)
    return app


def register_paths(app):
    """Register url paths and bind to views"""
    @app.route("/")
    def index():
        """Route to the root url"""
        return "Hello, World!"


def app_run():
    """Creates Flask instance and run it"""
    app = create_app()
    app.run()


if __name__ == "__main__":
    app_run()
    print('SHIT!')
