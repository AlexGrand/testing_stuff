# RD External Python Course Graduation Work ([PLAN](documentation/RD External Python Course Graduation Work.xlsx))

## Welcome to my external graduation work for EPAM.<br> Please feel yourself comfortable and don't hesitate to DM me ([Telegram](https://t.me/alex_grnd) :smiley:).
**Information about the project flow and it steps will be added just down here.**

### Tasks and steps:


- [x] In the root of the repository create folder "documentation". Create there a document Software Requirements Specification (SRS) with a description of a software system to be developed with all applicable use cases. The specification document must include forms mockups too. SRS has to be created in Markdown format in English.<br>
**RESULT**:<br>
[Software Requirements Specification](documentation/Specification.md)<br>
- [x] Create a static mockup version of the application in HTML format with hardcoded data. It must include a minimum of 4 pages: departments.html, department.html, employees.html, and employee.html. All pages have to include hyperlinks to simulate application use cases.
Inside ""documentation"" folder create folder ""html_prototype"" and push static prototype there.\
**RESULT:**\
Added static html mockups for web service (without css for now)\
[Telsa Inc - Departments](https://alexgrand.gitlab.io/rd-external-python-course-graduation-work/documentation/htmlPrototype/)<br>

- [x] Choose the project type: Flask application\
Choose Python technologies for working with database: SQLAlchemy\
**RESULT:**\
The next stack of technologies was chosen:\
`Flask` - webframework for fast web development\
`Flask-sqlalchemy` - for working with DB in ORM style\
`Flask-restful` - for REST approach in app\
`Unittest` - for testing\
`Flask-migrate` -for db migration

- [x] "Create a Python project with the following required structure:\
department-app (a project / app directory)\
|__ migrations (this package must include migration files to manage database schema changes )\
|__ models (this package must include modules with Python classes describing DB models (for ORM only))\
|__ service (this package must include modules with functions / classes to work with DB (CRUD operations))\
|__ sql (this folder must include *.sql files to work with DB (for non-ORM only))\
|__ rest (this package must include modules with RESTful service implementation)\
|__ templates (this folder must include web app html templates)\
|__ static (this folder must include static files (js, css, images, etc,))\
|__ tests (this package must include modules with unit tests)\
|__ views (this package must include modules with Web controllers / views)\
NOTES:
Project must include setup.py file - installation file telling Python how to install your project.
Any other project files / packages / sub-packages you might add in the future."\
**RESULT:**\
`Setup.py` file was made in declarative style:\
[setup config](/setup.cfg)\
[setup.py](/setup.py)

- [ ] Add the following to the project configuration:
Project build configuration should include “pylint” plugin
Use https://travis-ci.com for building the project on github\
Set up and add https://coveralls.io

- [ ] "Create database with the following requirements:
Create two tables: “department” and “employee”
Populate database with the test data
Departments should store their names
Employees should store the following data: related department, employee name, date of birth, salary\
NOTES:\
Configure your application to connect to the required db.
If you chosen ORM technology to work with db (SQLAlchemy), you should create models and then generate migration scripts to manage database schema changes . You should use special Python modules or corresponding features of the chosen technology to generate migration scripts automatically or manually based on created modules.
If you chosen non-ORM technology to work with db (mysql-connector-python), you should use special Python modules to run migration scripts which you should create manually.

- [ ] Create a web service (RESTful) for CRUD operations. One should be able to deploy the web service on Gunicorn using command line.  All public functions / methods on all levels should include unit tests. Debug information should be displayed at the debugging level in the console and in a separate file. Classes and functions / methods must have docstrings comments.

- [ ] Create a simple web application for managing departments and employees. The web application should use aforementioned web service for storing  data and reading from database.  One should be able to deploy the web application on Gunicorn using command line.  All public functions / methods on all levels should include unit tests. Debug information should be displayed at the debugging level in the console and in a separate file. Classes and functions / methods must have docstrings comments. Finalyze README file which should contain a brief description of the project, instructions on how to build a project from the command line, how to start it, and at what addresses the Web service and the Web application will be available after launch.\
The web application should allow:\
display a list of departments and the average salary (calculated automatically) for these departments
display a list of employees in the departments with an indication of the salary for each employee and a search field to search for employees born on a certain date or in the period between dates
change (add / edit / delete) the above data\
NOTE:\
This step may require updating existing or adding new REST endpoints to the aforementioned web service (if they were not taken into account in the previous step). For example, the implementation of employee search by date of birth or the addition of the possibility of calculating the average salary when getting a list of departments.









